#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "io_util.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char* hash_guess = md5(guess, strlen(guess));

    // Compare the two hashes
    int response = strcmp(hash, hash_guess) == 0 ? 1 : 0;

    // Free any malloc'd memory
    free(hash_guess);

    return response;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    // read the file into memory
    char* dictionary_data;
    int dictionary_length = get_file_data(filename, &dictionary_data);
    
    // parse and seperate the dictionary lines into an array
    char** dictionary_lines;
    *size = get_file_lines(dictionary_data, dictionary_length, &dictionary_lines);
    
    return dictionary_lines;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    // look I know it isn't a ""dictionary"", but this is easiest
    int hlen;
    char** hash = read_dictionary(argv[1], &hlen);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    for(int dentry = 0; dentry < dlen; dentry++) {
        for(int hentry = 0; hentry < hlen; hentry++) {
            if(tryguess(hash[hentry], dict[dentry])) {
                printf("HASH CRACKED: %s -> (%s)\n", hash[hentry], dict[dentry]);
                break;
            }
        }
    }
}
